#pragma once

#include <Arduino.h>
#include <FastLED.h>

void showWarning(
  CRGB color,                                               /* цвет вспышки                                                 */
  uint32_t duration,                                        /* продолжительность отображения предупреждения (общее время)   */
  uint16_t blinkHalfPeriod); 

void noTimeWarningShow();

#if defined(USE_RANDOM_SETS_IN_APP) || defined(RANDOM_SETTINGS_IN_CYCLE_MODE)
void setModeSettings(uint8_t Scale = 0U, uint8_t Speed = 0U);
#endif //#if defined(USE_RANDOM_SETS_IN_APP) || defined(RANDOM_SETTINGS_IN_CYCLE_MODE)

void drawPixelXYF(float x, float y, CRGB color);
void generateLine();
void shiftUp();
void drawFrame(uint8_t pcnt, bool isColored);

uint8_t wrapX(int8_t x);
uint8_t wrapY(int8_t y);
boolean fillString(const char* text, CRGB letterColor, boolean itsText);

void whiteColorStripeRoutine();
void colorRoutine();
void colorsRoutine2();
void madnessNoiseRoutine();
void cloudsNoiseRoutine();
void lavaNoiseRoutine();
void plasmaNoiseRoutine();
void rainbowNoiseRoutine();
void rainbowStripeNoiseRoutine();
void zebraNoiseRoutine();
void forestNoiseRoutine();
void oceanNoiseRoutine();
void BBallsRoutine();
void bounceRoutine();
void popcornRoutine();
void spiroRoutine();
void PrismataRoutine();
void smokeballsRoutine();
void execStringsFlame();
void Fire2021Routine();
void pacificRoutine();
void shadowsRoutine();
void DNARoutine();
void flockRoutine(bool);
void butterflysRoutine(bool);
void snakesRoutine();
void nexusRoutine();
void spheresRoutine();
void Sinusoid3Routine();
void MetaBallsRoutine();
void polarRoutine();
void spiderRoutine();
void LavaLampRoutine();
void LiquidLampRoutine(bool);
void newMatrixRoutine();
void matrixRoutine();
void fire2012again();
void Fire2018_2();
void fire2020Routine2();
void fireRoutine(bool);
void whirlRoutine(bool);
void magmaRoutine();
void LLandRoutine();
void fire2012WithPalette();
void fire2012WithPalette4in1();
void poolRoutine();
void pulseRoutine(uint8_t PMode);
void oscillatingRoutine();
void starfield2Routine();
void fairyRoutine();
void RainbowCometRoutine();
void ColorCometRoutine();
void MultipleStream();
void MultipleStream2();
void attractRoutine();
void MultipleStream3();
void MultipleStream5();
void MultipleStream8();
void sparklesRoutine();
void twinklesRoutine();
void MultipleStreamSmoke(bool);
void picassoSelector();
void WaveRoutine();
void sandRoutine();
void ringsRoutine();
void cube2dRoutine();
void simpleRain();
void stormyRain();
void coloredRain();
void RainRoutine();
void snowRoutine();
void stormRoutine2();
void LeapersRoutine();
void lightersRoutine();
void ballsRoutine();
void lumenjerRoutine();
void lightBallsRoutine();
void rainbowRoutine();
void clockRoutine();
void text_running();
