#pragma once
#include "Constants.h"
#include <Arduino.h>

time_t getCurrentLocalTime();

 #if defined(USE_NTP) || defined(USE_MANUAL_TIME_SETTING) || defined(GET_TIME_FROM_PHONE)
void timeTick();
void getFormattedTime(char *buf);
#endif

#ifdef USE_NTP
void resolveNtpServerAddress(bool &ntpServerAddressResolved);
#endif