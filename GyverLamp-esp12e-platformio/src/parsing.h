#pragma once
#include <Arduino.h>

void parseUDP();
void processInputBuffer(char *inputBuffer, char *outputBuffer, bool generateOutput);
void sendCurrent(char *outputBuffer);
void sendAlarms(char *outputBuffer);
void sendTimer(char *outputBuffer);
String getValue(String data, char separator, int index);