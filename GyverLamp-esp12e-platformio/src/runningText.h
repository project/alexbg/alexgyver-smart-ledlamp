#pragma once
#include <Arduino.h>
#include <FastLED.h>

void drawLetter(uint8_t letter, int8_t offset, CRGB letterColor);
uint8_t getBrightnessForPrintTime(uint32_t thisTime, bool ONflag);
uint8_t getFont(uint8_t asciiCode, uint8_t row);
void printTime(uint32_t thisTime, bool onDemand, bool ONflag);