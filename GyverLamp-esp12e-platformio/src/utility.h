#pragma once

#include<Arduino.h>
#include<FastLED.h>

uint16_t XY(uint8_t x, uint8_t y);
void fillAll(CRGB color);
float sqrt3(const float x);
void restoreSettings();

#if (WIDTH > 127) || (HEIGHT > 127)
void drawPixelXY(int16_t x, int16_t y, CRGB color);
#else
void drawPixelXY(int8_t x, int8_t y, CRGB color);
#endif

uint32_t getPixColorXY(uint8_t x, uint8_t y);
uint32_t getPixColor(uint16_t thisPixel);