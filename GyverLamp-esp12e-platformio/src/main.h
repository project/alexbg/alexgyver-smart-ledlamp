#pragma once

#include "Constants.h"
#include "Types.h"
#include <GyverButton.h>
#include <Arduino.h>
#include <WiFiManager.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <Timezone.h>

extern uint8_t espMode;
extern uint8_t currentMode;
extern CRGB leds[NUM_LEDS];
extern ModeType modes[MODE_AMOUNT];
extern bool loadingFlag;
extern bool timeSynched;
extern bool settChanged;
extern uint32_t eepromTimeout;
extern uint32_t lastTimePrinted;
extern bool ntpServerAddressResolved;

extern uint8_t dawnMode;
extern bool dawnFlag;
extern uint8_t dawnOffsets[];   // опции для выпадающего списка параметра "время перед 'рассветом'" (будильник); синхронизировано с android приложением

extern bool manualOff;
extern bool ONflag;
extern bool buttonEnabled; // это важное первоначальное значение. нельзя делать false по умолчанию

extern char* TextTicker;
extern int Painting; 
extern CRGB DriwingColor;
extern AlarmType alarms[7];
extern uint32_t thisTime;

#ifdef ESP_USE_BUTTON
extern GButton touch; // для физической (не сенсорной) кнопки HIGH_PULL. ну и кнопку нужно ставить без резистора в разрыв между пинами D2 и GND
#endif //ESP_USE_BUTTON

#if defined(USE_MANUAL_TIME_SETTING) || defined(GET_TIME_FROM_PHONE)
extern time_t manualTimeShift;
#endif

#ifdef GET_TIME_FROM_PHONE
extern time_t phoneTimeLastSync;
#endif

extern WiFiManager wifiManager;
extern WiFiServer wifiServer;
extern WiFiUDP Udp;

#ifdef USE_NTP
extern WiFiUDP ntpUDP;
extern  NTPClient timeClient; // объект, запрашивающий время с ntp сервера; в нём смещение часового пояса не используется (перенесено в объект localTimeZone); здесь всегда должно быть время UTC
  #ifdef SUMMER_WINTER_TIME
  extern Timezone localTimeZone;
  #else
  extern Timezone localTimeZone;
  #endif
  #ifdef PHONE_N_MANUAL_TIME_PRIORITY
    extern bool stillUseNTP;
  #endif    
#endif

#ifdef RANDOM_SETTINGS_IN_CYCLE_MODE
extern uint8_t random_on;
#endif //RANDOM_SETTINGS_IN_CYCLE_MODE

#if defined(USE_RANDOM_SETS_IN_APP) || defined(RANDOM_SETTINGS_IN_CYCLE_MODE)
extern uint8_t selectedSettings;
#endif //#if defined(USE_RANDOM_SETS_IN_APP) || defined(RANDOM_SETTINGS_IN_CYCLE_MODE)

#if defined(BUTTON_CAN_SET_SLEEP_TIMER) && defined(ESP_USE_BUTTON)
extern uint8_t button_sleep_time;
#endif //#if defined(BUTTON_CAN_SET_SLEEP_TIMER) && defined(ESP_USE_BUTTON)

extern unsigned char matrixValue[8][16]; //это массив для эффекта Огонь. что он тут делает? - хз